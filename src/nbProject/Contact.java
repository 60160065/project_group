package nbProject;

import java.util.*;

public class Contact {
	static Scanner kb = new Scanner(System.in);
	// for login panel
	static String username,password;
	static boolean foundID = false, foundPass = false;
	// for create list
	static ArrayList<User> arr = new ArrayList<User>();
	// for edit panel
	static User wait;
	static boolean foundEditPass = false;
	// for add panel
    static String addUser, addPass, addFname, addLname, editUser, editPass, editFname, editLname;
    static double addWeight, addHeight, editWeight, editHeight;


	public static void main(String[] args) {
		LoadData();
		ShowLogin();
	}

	public static void LoadData() {
		arr.add(new User("Payut", "Jurnangkarn","admin", "admin", 180, 80));
		arr.add(new User( "Phatcharapol", "Saenrang","ace", "ace", 175, 55));
	}
	// for message dialog

	public static void PrintErrorLogin() {
		System.err.println("Username or Password is incorrect!! , Please try again.");
	}
	
	public static void PrintSuccess() {
		System.out.println("Success.");
	}

	public static void showPleaseChoose() {
		System.out.print("Please choose : ");
	}

	public static void showErrorInput() {
		System.err.println("Input didn't match.");
	}

	public static void ShowErrorAddUser() {
		System.err.println("If Username has been exist. This message will show.");
	}
	public static void PrintErrorChoiceMenu() {
		System.err.println("Please input number between 1-5");
	}

	public static void showErrorZero() {
		System.err.println("Cannot add or edit with a zero");
	}

	public static void showErrorBlank() {
		System.err.println("Cannot add or edit with a blank");
	}

	public static void showErrorIndex() {
		System.err.println("Some field didn't fill.");
	}

	public static void showErrorShowInput() {
		System.err.println("Weight must be number.");
	}

	public static void showErrorWeight() {
		System.err.println("Weight must be number.");
	}

	public static void showErrorHeight() {
		System.err.println("Height must be number.");
	}

	public static void showErrorEditPass() {
		System.out.println("password didn't match");
	}

	// for Login menu
	public static void ShowLogin() {
		System.out.println("Please input your username and password");
		InputLogin();
		checkLogin(username, password);
	}


	private static void InputLogin() {
		System.out.print("Username : ");
		username = kb.next();
		System.out.print("Password : ");
		password = kb.next();
	}

	public static void checkLogin(String user, String password) {
		for (User a : arr) {
			if (user.equals(a.user)) {
				foundID = true;
				if (password.equals(a.getPass())) {
					foundPass = true;
					break;
				} else {
					foundPass = false;
					break;
				}
			} else {
				foundID = false;
			}
		}
		CheckLogin();
	}

	public static void CheckLogin() {
		if (foundID == true && foundPass == true) {
			PrintSuccess();
			ShowMenu();
		} else if (foundID == true && foundPass == false) {
			PrintErrorLogin();
			ShowLogin();
		} else {
			PrintErrorLogin();
			ShowLogin();
		}
	}

	// for menu panel
	public static void ShowMenu() {
		System.out.println("Menu");
		System.out.println("1. Register");
		System.out.println("2. Edit");
		System.out.println("3. ShowUserList");
		System.out.println("4. Logout");
		System.out.println("5. Exit");
		System.out.println("");
		checkMenu();
	}

	public static void checkMenu() {
		showPleaseChoose();
		char input = kb.next().charAt(0);
		switch (input) {
		case '1':
			ShowRegister();
			break;
		case '2':
			ShowEdit();
			break;
		case '3':
			ShowUserList();
			break;
		case '4':
			logOut();
			break;
		case '5':
			sayByebye();
			System.exit(0);
			break;
		default:
			PrintErrorChoiceMenu();
			ShowMenu();
		}
	}

	private static void sayByebye() {
		System.out.println("Bye bye...");
		
	}

	// for add panel
	public static void ShowRegister() {
		System.out.println("Please input information");
		System.out.println("1. Firstname 2. Lastname 3. Username 4. Password 5. Weight 6. Height");
		System.out.println("(#) back");
		CheckDetail();
	}
	
	public static void CheckDetail() {
		ChooseAddFname();
		ChooseAddLname();
		ChooseAddUser();
		ChooseAddPass();
		ChooseAddWeight();
		ChooseAddHeight();
		InputForSave();
	}

	public static void ChooseAddUser() {
		System.out.print("username : ");
		addUser = kb.next();
		if (addUser.equals("#")) {
			ShowMenu();
		} else {
			for (User a : arr) {
				if (a.getUser().equals(addUser)) {
					ShowErrorAddUser();
					ChooseAddUser();
				}
			}
			System.out.println("Username can use.");
		}
	}

	public static void ChooseAddWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight (cannot be zero) : ");
				String str = kb.next();
				if (str.equals("#")) {
					ShowMenu();
					break;
				}
				addWeight = Double.parseDouble(str);
				check = true;
			} catch (Exception e) {
				showErrorWeight();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void ChooseAddHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height (cannot be zero) : ");
				String str = kb.next();
				if (str.equals("#")) {
					ShowMenu();
					break;
				}
				addHeight = Double.parseDouble(str);
				check = true;
			} catch (Exception e) {
				showErrorHeight();
			}
		}
		System.out.println("Height can use.");
	}

	public static void ChooseAddPass() {
		System.out.print("password : ");
		addPass = kb.next();
		if (addPass.equals("#"))
			ShowMenu();
	}

	public static void ChooseAddFname() {
		System.out.print("firstname : ");
		addFname = kb.next();
		if (addFname.equals("#"))
			ShowMenu();
}

	public static void ChooseAddLname() {
		System.out.print("lastname : ");
		addLname = kb.next();
		if (addLname.equals("#"))
			ShowMenu();
}

	public static void InputForSave() {
		System.out.println("(#) back / (Y) save");
		showPleaseChoose();
		String input = kb.next();
		CheckSave(input);
	}

	public static void CheckSave(String input) {
		if (input.equalsIgnoreCase("Y")) {
            arr.add(new User(addFname, addLname ,addUser, addPass, addWeight, addHeight));
            PrintSuccess();
			ShowMenu();
		} else if (input.equals("#")) {
			ShowRegister();
		} else
			InputForSave();
	}

	public static void addSetDefault() {
	      addUser = "";
	      addPass = "";
	      addFname = "";
	      addLname = "";
	      addWeight = 0;
	      addHeight = 0;


	}

	// for show panel
	public static void ShowUserList() {
        System.out.println("UserList");
        for (int i = 0; i < arr.size(); i++) {
            System.out.println(i + 1 + ". " + arr.get(i).user);
        }
        System.out.println("(#) back");
        Show();
	}
	
	public static void Show() {
		boolean check = false;
		while (!check) {
			try {
				showPleaseChoose();
			    String inputChoose = kb.next();
				checkShow(inputChoose);

			} catch (InputMismatchException e) {
				showErrorShowInput();
				kb.next();
			}
		}
	}

    public static void checkShow(String inputChoose) {
    	try{
    	       for (int i = 0; i < arr.size(); i++) {
    	        	if (inputChoose.equals("#")) {
    	                ShowMenu();
    	                break;
    	        	}
    	        	int input = Integer.parseInt(inputChoose);
    	            if (input == i + 1) {
    	            	System.out.println("");
    	            	System.out.println("Detail");
    	                System.out.println(arr.get(i));
    	                System.out.println("");
    	                ShowUserList();
    	                break;
    	            } else if (input > arr.size()) {
    	                showErrorInput();
    	                ShowUserList();
    	                break;
    	            }
    	        }
    	        showErrorInput();
    	        ShowUserList();
    	}catch(Exception e){
    		showErrorInput();
	        ShowUserList();
    	}

    }

	// for edit panel
	public static void ShowEdit() {
			for (User a : arr) {
					wait = new User(a.getUser(), a.getPass(), a.getF(), a.getL(), a.getW(), a.getH());
					CheckEdit();
			}
	}



	public static void CheckEdit( ) {
		InputChoiceEdit();
		char inputEdit = kb.next().charAt(0);
		if (inputEdit=='#') {
			ShowMenu();
		} else if (inputEdit=='2') {
			chooseEditPass();
			CheckEdit();
		} else if (inputEdit=='3') {
			chooseEditFname();
			CheckEdit();
		} else if (inputEdit=='4') {
			chooseEditLname();
			CheckEdit();
		} else if (inputEdit=='5') {
			chooseEditWeight();
			CheckEdit();
		} else if (inputEdit=='6') {
			chooseEditHeight();
			CheckEdit();
		} else if (inputEdit=='Y') {
			editSave();
		} else {
			showErrorInput();
			CheckEdit();
		}

	}

	public static void InputChoiceEdit() {
		System.out.println("Edit Panel");
		System.out.println("1. Username: " + wait.getUser() + " 2. Password: " + wait.getPass() + " 3. Firstname: " + wait.getF()
						+ " 4. Lastname: " + wait.getL() + " 5. Weight: " + wait.getW() + " 6. Height: " + wait.getH());
		System.out.println("(#) back / (Y)save");
		System.err.println("Username can't change!");
	}

	public static void editSave() {
		for (User a : arr) {
			if (a.getUser().equals(wait.user)) {
				a.setPass(wait.getPass());
				a.setF(wait.getF());
				a.setL(wait.getL());
				a.setW(wait.getW());
				a.setH(wait.getH());
			}
		}
		PrintSuccess();
		ShowMenu();
	}

	public static void chooseEditWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight : ");
				String str = kb.next();
				if (str.equals("#")) {
					CheckEdit();
					break;
				}
				editWeight = Double.parseDouble(str);
				wait.setW(editWeight);
				check = true;
			} catch (Exception e) {
				showErrorWeight();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseEditHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height : ");
				String str = kb.next();
				if (str.equals("#")) {
					CheckEdit();
					break;
				}
				editHeight = Double.parseDouble(str);
				wait.setH(editHeight);
				check = true;
			} catch (Exception e) {
				showErrorHeight();
			}
		}
		System.out.println("Height can use.");
	}

	public static void chooseEditPass() {
		System.out.print("password : ");
		editPass = kb.next();
		if (editPass.equals("#"))
			CheckEdit();
		wait.setPass(editPass);
	}

	public static void chooseEditFname() {
		System.out.print("firstname : ");
		editFname = kb.next();
		if (editFname.equals("#"))
			CheckEdit();
		wait.setF(editFname);
	}

	public static void chooseEditLname() {
		System.out.print("lastname : ");
		editLname = kb.next();
		if (editLname.equals("#"))
			CheckEdit();
		wait.setL(editLname);
	}

	// for logout
	public static void logOut() {
		showPleaseChoose();
		ShowLogin();
		logOut();

	}
}
//POJO
class User {
	String user;
	String pass;
	String fname;
	String lname;
	double weight;
	double height;

	public User( String fname, String lname,String user, String pass, double weight, double height) {
		this.fname = fname;
		this.lname = lname;
		this.user = user;
		this.pass = pass;
		this.weight = weight;
		this.height = height;
	}

	public String toString() {
		String p1 = "\nFirstname : " + fname;
		String p2 = "\nLastname : " + lname;
		String p3 = "Username : " + user;
		String p4 = "\nPassword : " + pass;
		String p5 = "\nWeight : " + Double.toString(weight);
		String p6 = "\nHeight : " + Double.toString(height);
		return p1 + p2 + p3 + p4 + p5 + p6;

	}

	public String getF() {
		return fname;
	}

	public String getL() {
		return lname;
	}
	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public Double getW() {
		return weight;
	}

	public Double getH() {
		return height;
	}
	
	public void setF(String fname) {
		this.fname = fname;
	}

	public void setL(String lname) {
		this.lname = lname;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setW(double weight) {
		this.weight = weight;
	}

	public void setH(double height) {
		this.height = height;
	}
}

